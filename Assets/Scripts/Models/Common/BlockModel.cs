using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockModel:MonoBehaviour
{
    /// <summary>
    /// 编号
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// 名称
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// 当前位置存在的单元信息
    /// </summary>
    public BaseCellModel Cell { get; set; }

}
