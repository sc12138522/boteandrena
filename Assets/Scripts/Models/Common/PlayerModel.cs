using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerModel : BaseCellModel, IBlockAction
{

    #region Instance

    private PlayerModel()
    {
        this.Name = "Player";

    }

    private static readonly object lockObj = new object();
    private static PlayerModel instance;
    public static PlayerModel Instance
    {
        get
        {
            if (instance == null)
            {
                lock (lockObj)
                {
                    instance = new PlayerModel();
                }
            }

            return instance;
        }
    }


    #endregion

    public List<E_TiggerType> TigerList { get; set; }

    public override E_BlockType Type
    {
        get { return E_BlockType.Player; }
    }

    public void OnClick()
    {
        throw new System.NotImplementedException();
    }
}
