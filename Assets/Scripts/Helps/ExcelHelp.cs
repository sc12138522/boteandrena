﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;

#region Enum

/// <summary>
/// 单元格类型
/// </summary>
public enum E_CellType
{
    /// <summary>
    /// 默认
    /// </summary>
    None = 0,
    /// <summary>
    /// 字符串
    /// </summary>
    String = 1,
    /// <summary>
    /// 数字
    /// </summary>
    Number = 2,
    /// <summary>
    /// 时间
    /// </summary>
    Time = 3,
}

/// <summary>
/// xls样式
/// </summary>
public enum E_XlsStyle
{
    /// <summary>
    /// 默认
    /// </summary>
    None = 0,
    /// <summary>
    /// 头标题
    /// </summary>
    Head = 1,
    /// <summary>
    /// 链接
    /// </summary>
    Url = 2,
    /// <summary>
    /// 时间
    /// </summary>
    Time = 3,
    /// <summary>
    /// 数字
    /// </summary>
    Number = 4,
    /// <summary>
    /// 钱
    /// </summary>
    Money = 5,
    /// <summary>
    /// 百分比
    /// </summary>
    Percent = 6,
    /// <summary>
    /// 大写中文
    /// </summary>
    CheseUpper = 7,
    /// <summary>
    /// 科学计数法
    /// </summary>
    ScientNotation = 8,
    /// <summary>
    /// 列标题
    /// </summary>
    ColumnHead = 9,

}

#endregion

public static class ExcelHelp
{
    /// <summary>
    /// Excel转DataTable
    /// </summary>
    /// <param name="filePath"></param>
    /// <param name="isColuName"></param>
    /// <returns></returns>
    public static DataTable ExcelToDataTable(string filePath, bool isColuName)
    {
        DataTable dt = null;
        FileStream fs = null;
        IWorkbook wb = null;
        ISheet sheet = null;
        int startRow = 0;

        try
        {
            using (fs = File.OpenRead(filePath))
            {

                if (filePath.IndexOf(".xlsx") > 0)
                {
                    wb = new XSSFWorkbook(fs);
                }
                else if (filePath.IndexOf(".xls") > 0)
                {
                    wb = new HSSFWorkbook(fs);
                }

                if (wb != null)
                {
                    sheet = wb.GetSheetAt(0);
                    dt = new DataTable();

                    if (sheet != null)
                    {
                        int rowCount = sheet.LastRowNum;
                        if (rowCount > 0)
                        {
                            IRow firstRow = sheet.GetRow(0);
                            int cellCount = firstRow.LastCellNum;
                            DataColumn colum;
                            ICell cell;
                            if (isColuName)
                            {
                                startRow = 1;
                                for (int i = firstRow.FirstCellNum; i < cellCount; i++)
                                {
                                    cell = firstRow.GetCell(i);
                                    if (cell != null)
                                    {
                                        if (cell.StringCellValue != null)
                                        {
                                            colum = new DataColumn(cell.StringCellValue);
                                            dt.Columns.Add(colum);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                for (int i = firstRow.FirstCellNum; i < cellCount; i++)
                                {
                                    colum = new DataColumn("column" + (i + 1));
                                    dt.Columns.Add(colum);
                                }
                            }

                            for (int i = startRow; i < rowCount + startRow; i++)
                            {
                                IRow row = sheet.GetRow(i);
                                if (row == null)
                                {
                                    continue;
                                }
                                DataRow datarow = dt.NewRow();
                                try
                                {
                                    for (int j = row.FirstCellNum; j < cellCount; j++)
                                    {
                                        cell = row.GetCell(j);
                                        if (cell == null)
                                        {
                                            datarow[j] = "";
                                        }
                                        else
                                        {
                                            switch (cell.CellType)
                                            {
                                                case CellType.Unknown:
                                                    break;
                                                case CellType.Numeric:
                                                    bool isExcelData = DateUtil.IsCellDateFormatted(cell);
                                                    if (isExcelData)
                                                    {
                                                        datarow[j] = cell.DateCellValue;
                                                    }
                                                    else
                                                    {
                                                        datarow[j] = cell.NumericCellValue;
                                                    }
                                                    break;
                                                case CellType.String:

                                                    datarow[j] = cell.StringCellValue;
                                                    break;
                                                case CellType.Formula:
                                                    break;
                                                case CellType.Blank:
                                                    datarow[j] = "";
                                                    break;
                                                case CellType.Boolean:
                                                    break;
                                                case CellType.Error:
                                                    break;
                                                default:
                                                    break;
                                            }
                                        }
                                    }

                                    dt.Rows.Add(datarow);
                                }
                                catch (Exception)
                                {

                                    throw;
                                }
                            }

                        }
                    }
                }

            }

            return dt;
        }
        catch (Exception)
        {
            if (fs != null)
            {
                fs.Close();
            }

            throw;
        }
    }

    /// <summary>
    /// DataTable转Excel
    /// </summary>
    /// <param name="dt"></param>
    /// <param name="filePath"></param>
    /// <returns></returns>
    public static bool DataTableToExcel(DataTable dt, string filePath)
    {
        bool result = false;
        FileStream fs = null;
        try
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                IWorkbook wb;
                if (filePath.Contains(".xlsx"))
                {
                    wb = new XSSFWorkbook();
                }
                else
                {
                    wb = new HSSFWorkbook();
                }
                ISheet sheet = wb.CreateSheet("Sheet0");
                int rowCount = dt.Rows.Count;
                int colCount = dt.Columns.Count;
                IRow row = sheet.CreateRow(0);

                ICell cell;
                for (int i = 0; i < colCount; i++)
                {
                    cell = row.CreateCell(i);
                    cell.SetCellValue(dt.Columns[i].ColumnName);
                }
                for (int i = 0; i < rowCount; i++)
                {
                    row = sheet.CreateRow(i + 1);
                    for (int j = 0; j < colCount; j++)
                    {
                        cell = row.CreateCell(j);
                        cell.SetCellValue(dt.Rows[i][j].ToString());
                    }
                }

                using (fs = File.OpenWrite(filePath))
                {
                    wb.Write(fs);
                    result = true;
                }



            }

            return result;
        }
        catch (Exception)
        {
            if (fs != null)
            {
                fs.Close();
            }

            throw;
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="fileFullName">文件保存的地址</param>
    /// <param name="sheetName">页标题</param>
    /// <param name="titleName">标题名称</param>
    /// <param name="columnName">列名称</param>
    /// <param name="data">数据值</param>
    /// <param name="errMsg">错误信息</param>
    /// <returns></returns>
    public static bool ExporXls(string fileFullName, string sheetName, string titleName, string[] columnName, string[,] data, out string errMsg)
    {
        errMsg = string.Empty;

        try
        {
            var xlsx = fileFullName.Contains(".xlsx");
            IWorkbook wb = xlsx ? new XSSFWorkbook() as IWorkbook : new HSSFWorkbook() as IWorkbook;
            ISheet sheet = wb.CreateSheet(sheetName);
            var starRow = 0;

            if (!string.IsNullOrEmpty(titleName))
            {
                IRow row = sheet.CreateRow(starRow);
                row.Height = 25 * 20;
                ICell cell1 = row.CreateCell(0);
                cell1.CellStyle = GetCellStyle(wb, E_XlsStyle.Head);
                cell1.SetCellValue(titleName);
                sheet.AddMergedRegion(new CellRangeAddress(0, 0, 0, columnName.Length - 1));
                starRow++;

            }

            if (columnName != null)
            {
                IRow row = sheet.CreateRow(starRow);
                for (int i = 0; i < columnName.Length; i++)
                {
                    ICell cell = row.CreateCell(i);
                    cell.CellStyle = GetCellStyle(wb, E_XlsStyle.ColumnHead);
                    cell.SetCellValue(columnName[i]);
                }
            }
            else
            {
                errMsg = "无标题列";
                return false;
            }

            if (data != null)
            {
                starRow++;
                for (int i = 0; i < data.GetLength(0); i++)
                {
                    var row = sheet.CreateRow(starRow);
                    for (int j = 0; j < data.GetLength(1); j++)
                    {
                        ICell cell = row.CreateCell(j);
                        cell.SetCellValue(data[i, j]);

                    }
                    starRow++;


                }

            }
            else
            {
                errMsg = "无数据列";

                return false;
            }

            using (MemoryStream ms = new MemoryStream())
            {
                wb.Write(ms);
                ms.Flush();
                using (var fs = new FileStream(fileFullName, FileMode.Create, FileAccess.Write))
                {
                    var saveData = ms.ToArray();
                    fs.Write(saveData, 0, saveData.Length);
                    fs.Flush();

                }
            }

        }
        catch (Exception ex)
        {
            errMsg = ex.Message;

            return false;
        }

        return true;
    }

    /// <summary>
    /// 导出文件
    /// </summary>
    /// <param name="fileFullName">文件保存的地址</param>
    /// <param name="sheetName">页标题</param>
    /// <param name="titleName">标题名称</param>
    /// <param name="columnName">列名称</param>
    /// <param name="columnCellEnumType">类型</param>
    /// <param name="data">数据值</param>
    /// <param name="errMsg">错误信息</param>
    /// <returns></returns>
    public static bool ExporXls(string fileFullName, string sheetName, string titleName, string[] columnName, E_CellType[] columnCellEnumType, object[,] data, out string errMsg)
    {
        errMsg = string.Empty;

        try
        {
            var xlsx = fileFullName.Contains(".xlsx");
            IWorkbook wb = xlsx ? new XSSFWorkbook() as IWorkbook : new HSSFWorkbook() as IWorkbook;
            ISheet sheet = wb.CreateSheet(sheetName);
            var starRow = 0;

            //标题
            if (!string.IsNullOrEmpty(titleName))
            {
                IRow row = sheet.CreateRow(starRow);
                row.Height = 25 * 20;
                ICell cell1 = row.CreateCell(0);
                cell1.CellStyle = GetCellStyle(wb, E_XlsStyle.Head);
                cell1.SetCellValue(titleName);
                sheet.AddMergedRegion(new CellRangeAddress(0, 0, 0, columnName.Length - 1));
                starRow++;

            }

            //列标题
            if (columnName != null)
            {
                IRow row = sheet.CreateRow(starRow);
                for (int i = 0; i < columnName.Length; i++)
                {
                    ICell cell = row.CreateCell(i);
                    cell.CellStyle = GetCellStyle(wb, E_XlsStyle.ColumnHead);
                    cell.SetCellValue(columnName[i]);
                }
            }
            else
            {
                errMsg = "无标题列";
                return false;
            }

            //数据行
            if (data != null)
            {
                starRow++;
                for (int i = 0; i < data.GetLength(0); i++)
                {
                    var row = sheet.CreateRow(starRow);
                    for (int j = 0; j < data.GetLength(1); j++)
                    {
                        E_CellType cellType = columnCellEnumType[j];

                        ICell cell = null;

                        switch (cellType)
                        {
                            case E_CellType.String:
                                cell = row.CreateCell(j, CellType.String);
                                cell.SetCellValue(data[i, j].ToString());
                                break;
                            case E_CellType.Number:
                                cell = row.CreateCell(j, CellType.Numeric);
                                cell.SetCellValue(Convert.ToDouble(data[i, j]));
                                break;
                            case E_CellType.Time:
                                cell = row.CreateCell(j, CellType.Numeric);
                                cell.SetCellValue(Convert.ToDateTime(data[i, j]));
                                break;
                            default:
                                cell = row.CreateCell(j, CellType.String);
                                cell.SetCellValue((string)data[i, j].ToString());
                                break;
                        }
                    }
                    starRow++;
                }
            }
            else
            {
                errMsg = "无数据列";

                return false;
            }

            using (MemoryStream ms = new MemoryStream())
            {
                wb.Write(ms);
                ms.Flush();
                using (var fs = new FileStream(fileFullName, FileMode.Create, FileAccess.Write))
                {
                    var saveData = ms.ToArray();
                    fs.Write(saveData, 0, saveData.Length);
                    fs.Flush();

                }
            }

        }
        catch (Exception ex)
        {
            errMsg = ex.Message;

            return false;
        }

        return true;
    }

    /// <summary>
    /// 导出文件
    /// </summary>
    /// <param name="fileFullName">文件保存的地址</param>
    /// <param name="sheetName">页标题</param>
    /// <param name="titleName">标题名称</param>
    /// <param name="isAppend">是否为追加</param>
    /// <param name="startRowIndex">追加位置起始行</param>
    /// <param name="columnName">列名称</param>
    /// <param name="data">数据值</param>
    /// <param name="errMsg">错误信息</param>
    /// <returns></returns>
    public static bool ExporXls(string fileFullName, string sheetName, string titleName, bool isAppend, int startRowIndex, string[] columnName, string[,] data, out string errMsg)
    {
        errMsg = string.Empty;

        try
        {
            var xlsx = fileFullName.Contains(".xlsx");
            IWorkbook wb = null;
            ISheet sheet = null;

            var starRow = 0;//起始行

            if (isAppend)
            {
                FileStream file = new FileStream(fileFullName, FileMode.Open, FileAccess.Read);
                if (xlsx)
                {
                    wb = new XSSFWorkbook(file);
                }
                else
                {
                    wb = new HSSFWorkbook(file);
                }
                sheet = wb.GetSheetAt(0);
                starRow = startRowIndex;
            }
            else
            {
                wb = xlsx ? new XSSFWorkbook() : new HSSFWorkbook() as IWorkbook;
                sheet = wb.CreateSheet(sheetName);

                //标题
                if (!string.IsNullOrEmpty(titleName))
                {
                    IRow row = sheet.CreateRow(starRow);
                    row.Height = 25 * 20;
                    ICell cell1 = row.CreateCell(0);
                    cell1.CellStyle = GetCellStyle(wb, E_XlsStyle.Head);
                    cell1.SetCellValue(titleName);
                    sheet.AddMergedRegion(new CellRangeAddress(0, 0, 0, columnName.Length - 1));
                    starRow++;

                }

                //列标题
                if (columnName != null)
                {
                    IRow row = sheet.CreateRow(starRow);
                    for (int i = 0; i < columnName.Length; i++)
                    {
                        ICell cell = row.CreateCell(i);
                        cell.CellStyle = GetCellStyle(wb, E_XlsStyle.ColumnHead);
                        cell.SetCellValue(columnName[i]);
                    }
                }
                else
                {
                    errMsg = "无标题列";
                    return false;
                }

                starRow++;
            }

            //数据行
            if (data != null)
            {
                starRow++;
                for (int i = 0; i < data.GetLength(0); i++)
                {
                    var row = sheet.CreateRow(starRow);
                    for (int j = 0; j < data.GetLength(1); j++)
                    {
                        ICell cell = row.CreateCell(j);
                        cell.SetCellValue(data[i, j]);

                    }
                    starRow++;


                }

            }
            else
            {
                errMsg = "无数据列";

                return false;
            }


            using (MemoryStream ms = new MemoryStream())
            {
                wb.Write(ms);
                ms.Flush();
                using (var fs = new FileStream(fileFullName, FileMode.Create, FileAccess.Write))
                {
                    var saveData = ms.ToArray();
                    fs.Write(saveData, 0, saveData.Length);
                    fs.Flush();

                }
            }

        }
        catch (Exception ex)
        {
            errMsg = ex.Message;

            return false;
        }

        return true;
    }

    /// <summary>
    /// 设置单元格样式
    /// </summary>
    /// <param name="wb"></param>
    /// <param name="str"></param>
    /// <returns></returns>
    private static ICellStyle GetCellStyle(IWorkbook wb, E_XlsStyle str)
    {
        ICellStyle cellStyle = wb.CreateCellStyle();
        IFont font12 = wb.CreateFont();
        font12.FontHeightInPoints = 10;
        font12.FontName = "微软雅黑";
        IFont font = wb.CreateFont();
        font.FontName = "微软雅黑";
        IFont fontColorBlue = wb.CreateFont();
        fontColorBlue.IsItalic = true;
        fontColorBlue.FontName = "微软雅黑";

        cellStyle.WrapText = true;
        cellStyle.Indention = 0;
        switch (str)
        {
            case E_XlsStyle.Head:
                var headFont = wb.CreateFont();
                headFont.FontHeight = 25;
                headFont.FontName = "微软雅黑";
                headFont.FontHeightInPoints = 18;
                cellStyle.Alignment = HorizontalAlignment.Center;
                cellStyle.VerticalAlignment = VerticalAlignment.Center;
                cellStyle.SetFont(headFont);
                break;
            case E_XlsStyle.Url:
                cellStyle.SetFont(fontColorBlue);
                break;
            case E_XlsStyle.Time:
                IDataFormat dataStyle = wb.CreateDataFormat();
                cellStyle.DataFormat = dataStyle.GetFormat("yyyy/MM/dd HH:mm:ss.fff");
                cellStyle.SetFont(font);
                break;
            case E_XlsStyle.Number:
                cellStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("0.00");
                cellStyle.SetFont(font);
                break;
            case E_XlsStyle.Money:
                IDataFormat format = wb.CreateDataFormat();
                cellStyle.DataFormat = format.GetFormat("¥#，##0");
                cellStyle.SetFont(font);
                break;
            case E_XlsStyle.Percent:
                cellStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("0.00%");
                cellStyle.SetFont(font);
                break;
            case E_XlsStyle.CheseUpper:
                IDataFormat format1 = wb.CreateDataFormat();
                cellStyle.DataFormat = format1.GetFormat("[DbNum2][$-804]0");
                cellStyle.SetFont(font);
                break;
            case E_XlsStyle.ScientNotation:
                cellStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("0.00E+00");
                cellStyle.SetFont(font);
                break;
            case E_XlsStyle.ColumnHead:
                var blodFont = wb.CreateFont();
                blodFont.Boldweight = 2;
                cellStyle.SetFont(blodFont);
                break;
            case E_XlsStyle.None:
                cellStyle.SetFont(font);
                break;
            default:
                break;
        }

        return cellStyle;

    }

}


