using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum E_BlockType
{
    /// <summary>
    /// 空
    /// </summary>
    None = 0,
    /// <summary>
    /// 玩家
    /// </summary>
    Player = 1,
    /// <summary>
    /// 伙伴
    /// </summary>
    Friend = 2,
    /// <summary>
    /// 石块
    /// </summary>
    Stone = 3,
    /// <summary>
    /// 花
    /// </summary>
    Flower = 4,
    /// <summary>
    /// 水泡
    /// </summary>
    Bubble = 5,
    /// <summary>
    /// 蘑菇
    /// </summary>
    Mashroom = 6,
    /// <summary>
    /// 入口
    /// </summary>
    Entrance = 7,
    /// <summary>
    /// 出口
    /// </summary>
    Exit = 8,

}
