using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FriendModel : BaseCellModel, IBlockAction
{

    public List<E_TiggerType> TigerList { get; set; }
    public override E_BlockType Type
    {
        get { return E_BlockType.Friend; }
    }

    public void OnClick()
    {
        throw new System.NotImplementedException();
    }
}
