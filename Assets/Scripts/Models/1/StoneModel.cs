using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneModel:BaseCellModel,IBlockAction
{
    public StoneModel()
    { 
        
    }

    public override E_BlockType Type
    {
        get { return E_BlockType.Stone; }
    }

    public void OnClick()
    {
        throw new System.NotImplementedException();
    }
}
