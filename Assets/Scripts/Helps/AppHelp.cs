using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppHelp
{

    #region Instance

    private AppHelp()
    {

    }

    private static readonly object lockObj = new object();
    private static AppHelp instance;
    public static AppHelp Instance
    {
        get
        {
            if (instance == null)
            {
                lock (lockObj)
                {
                    instance = new AppHelp();
                }
            }

            return instance;
        }
    }

    #endregion

    /// <summary>
    /// Assets路径地址
    /// </summary>
    public string AssetsPath
    {
        get { return Application.dataPath; }
    }

    /// <summary>
    /// 地图配置的路径
    /// </summary>
    public string MapInfoPath
    {
        get { return AssetsPath + @"\Data\Excel\"; }
    }



}
