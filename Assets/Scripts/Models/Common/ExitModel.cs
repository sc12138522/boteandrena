using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitModel : BaseCellModel
{
    public override E_BlockType Type
    {
        get { return E_BlockType.Exit; }
    }
}
