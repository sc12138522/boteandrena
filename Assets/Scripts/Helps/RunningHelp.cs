using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningHelp 
{

    #region Instance

    private RunningHelp()
    {

    }

    private static readonly object lockObj = new object();
    private static RunningHelp instance;
    public static RunningHelp Instance
    {
        get
        {
            if (instance == null)
            {
                lock (lockObj)
                {
                    instance = new RunningHelp();
                }
            }

            return instance;
        }
    }

    #endregion

    /// <summary>
    /// 地图集合
    /// </summary>
    public BlockModel[,] MapList { get; set;}

    /// <summary>
    /// 初始化
    /// </summary>
    public void ApplicationInit()
    {
        CreateMap(8, 10);

    }

    /// <summary>
    /// 创建地图
    /// </summary>
    /// <param name="row"></param>
    /// <param name="colum"></param>
    private void CreateMap(int row,int colum)
    { 
        MapList = new BlockModel[row,colum];

        int sum = 0;
        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < colum; j++)
            {
                GameObject obj = new GameObject("Block");
                GenerateSprite(obj.AddComponent<SpriteRenderer>(), Color.white);
                obj.transform.position = new Vector2(i, j);

                BlockModel bm = obj.AddComponent<BlockModel>();
                bm.Id = sum++;
                bm.Name = "B_" + sum;

                MapList[i, j] = bm;
            }
        }
    }

    private void CreateCell()
    {
        //从数据表中读取地图信息
    }


    /// <summary>
    /// 设置SpriteRenderer
    /// </summary>
    /// <param name="sr"></param>
    /// <param name="color"></param>
    private void GenerateSprite(SpriteRenderer sr, Color color)
    {
        Texture2D t = new Texture2D(90, 90);
        for (int w = 0; w < t.width; w++)
        {
            for (int h = 0; h < t.height; h++)
            {
                t.SetPixel(w, h, color);
            }
        }
        t.Apply();

        Sprite pic = Sprite.Create(t, new Rect(0, 0, t.width, t.height), new Vector2(0.5f, 0.5f));

        sr.sprite = pic;
    }

}
