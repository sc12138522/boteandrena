using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseCellModel
{
    /// <summary>
    /// ID
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// 名称
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// 类型
    /// </summary>
    public abstract E_BlockType Type { get; }

    /// <summary>
    /// 位置信息
    /// </summary>
    public Vector2 Position { get; set; }

    /// <summary>
    /// 面朝向
    /// </summary>
    public Vector2 Direction { get; set; }

    /// <summary>
    /// 是否不可用
    /// </summary>
    public bool IsDie { get; set; }

    /// <summary>
    /// 是否允许穿过
    /// </summary>
    public bool IsAllowPass { get; set; }

    

    
}
