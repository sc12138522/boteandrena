using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntranceModel : BaseCellModel
{
    public override E_BlockType Type
    {
        get { return E_BlockType.Entrance; }
    }

}
